# Bachelor Thesis Scripts

This repository will soon comprise all scripts to reproduce the results of my bachelor's thesis "Structural and functional analysis of cyanobacterial metabolic networks - how does functionality compare to phylogeny?".